# State-Space Post-covid Forecasting

This repository yields the results presented in the article [State-Space Models for Online Post-Covid Electricity Load Forecasting Competition](https://ieeexplore.ieee.org/document/9677626), published in *IEEE Open Access Journal of Power and Energy*.

The submissions of this repository were designed by [Joseph de Vilmarest](https://josephdevilmarest.github.io/) and [Yannig Goude](https://www.imo.universite-paris-saclay.fr/~goude/about.html) to win the competition [Day-Ahead Electricity Demand Forecasting: Post-COVID Paradigm](https://ieee-dataport.org/competitions/day-ahead-electricity-demand-forecasting-post-covid-paradigm). It relies on a time series forecasting algorithm designed by Joseph de Vilmarest and [Olivier Wintenberger](http://wintenberger.fr/).

Licence: LGPL.


## Structure of the repository

The competition data set is in the *data_raw* folder, the processed data files are in the *data* folder. The submissions used to win the competition are in the *submissions* folder.

The scripts yielding the results of the article are in the *R* and *python* folders. Furthermore here is a description of the scripts that are run in the following order:
- *R/create_data.R* reads the raw data set,
- *R/correct_weather.R* applies the correction of the meteorological variables,
- *R/mlp_create_variables.R* creates the files to train the MLP,
- *python/mlp.py* trains the MLP using keras,
- *R/mlp.R* creates the design variable X used for the state-space adaptation of MLP,
- *R/generate_experts.R* generates the experts,
- *R/correct_intraday.R* applies the intraday correction to the experts,
- *R/aggregation.R* contains the different aggregation procedures.

Finally *plots/plots_article.R* is a script generating the plots of the article.