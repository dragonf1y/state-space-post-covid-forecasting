data <- readRDS('data/data_corr.RDS')
sel <- which(data$BH==1)
sel2 <- c(sel-24, sel, sel+24)
length(sel2)
data <- data[-sel2,]

train <- which(data$Time < as.POSIXct(strptime("2020-01-01 00:00:00", "%Y-%m-%d %H:%M:%S"), tz="UTC"))

y <- data$Load
X <- matrix(data.matrix(data[,c(28,27,31,32,42)]), nrow(data), 5)
for (h in 0:7) {
  sel <- which(data$Hour == h)
  X[sel,3] <- data$Load1D[sel]
}
for (i in 1:6)
  X <- cbind(X, 1*(data$DayType==i))

for (h in 0:23) {
  sel <- which(data$Hour == h)
  sel_train <- which(data$Hour[train] == h)
  y[sel] <- (y[sel] - min(y[sel_train])) / (max(y[sel_train]) - min(y[sel_train]))
  for (j in 1:ncol(X)) 
    X[sel,j] <- (X[sel,j] - mean(X[sel_train,j])) / sd(X[sel_train,j])
}
X <- cbind(X,1)
d <- ncol(X)

write.csv(X[train,], 'data/Xcorr.csv', row.names=F)
write.csv(y[train], 'data/y.csv', row.names=F)