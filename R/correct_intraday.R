source('R/utils.R')
prev <- readRDS('data/experts.RDS')
prev_corr <- prev
for (j in 4:ncol(prev)) {
  res <- prev$Load - prev[,j]
  X <- matrix(0, nrow(prev), 24)
  for (t in 1:nrow(prev))
    X[t,] <- res[24*max(0,(t-1)%/%24-2)+9:32]
  d <- ncol(X)
  
  reshat.static <- numeric(nrow(prev))
  for (h in 0:23) {
    sel <- which(prev$Hour == h)
    reshat.static[sel] <- predict_online(list(theta=matrix(0,d,1),P=diag(d)),
                                         X[sel,], res[sel], delay=if (h < 8) 1 else 2)
  }
  prev_corr[,j] <- prev[,j] + reshat.static
}
saveRDS(prev_corr, 'data/experts_corr.RDS')

test5 <- which(prev$Time >= as.POSIXct(strptime("2021-01-18 00:00:00", "%Y-%m-%d %H:%M:%S"), tz="UTC"))
for (j in 4:ncol(prev)) {
  print(c(names(prev)[j],mae(prev$Load[test5],prev[test5,j]),
          mae(prev$Load[test5],prev_corr[test5,j])))
}


